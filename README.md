# Media expire Email

This module enables you to opt of email alert and send emails to the respective media authors before expiry based on set interval.
Instructions:
 - "Enable Media Expire Email Alert" on admin/config/media-expire/email
 - Set Email subject and Email message
 - Set single or multiple intervals in days for getting email alert ex: 1 day, 3 days, 7 days, 14 days, 31 days before media expire.

Installation:
 - Should be enabled with Media Expire module.
 - Follow normal installation process.
	
Drupal checks on every cron-run if there are media elements which are about to expire in set interval days as per the current time and trigger email to media author. 
