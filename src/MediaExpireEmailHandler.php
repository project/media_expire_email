<?php

namespace Drupal\media_expire_email;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class Drupal\media_expire_email\MediaExpireEmailHandler.
 */
class MediaExpireEmailHandler {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Media expire configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $mediaExpireEmailSettings;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The state factory.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructor for \Drupal\media_expire_email\MediaExpireEmailHandler class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   A state variable.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactoryInterface $config_factory,
                              MailManagerInterface $mail_manager,
                              LoggerChannelFactoryInterface $logger_factory,
                              StateInterface $state,
                              DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mediaExpireEmailSettings = $config_factory
      ->get('media_expire_email.settings');
    $this->mailManager = $mail_manager;
    $this->loggerFactory = $logger_factory;
    $this->state = $state;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Send Email for media to be expired before the set interval.
   */
  public function sendExpiryEmail() {
    $email_alert_enabled = $this->mediaExpireEmailSettings
      ->get('media_expire_email_enabled');

    // No action if it is not enabled.
    if (!$email_alert_enabled) {
      $this->setLastRunCheck();
      return;
    }

    $interval_ranges = $this->getIntervalRange();

    if (empty($interval_ranges)) {
      $this->setLastRunCheck();
      return;
    }

    foreach ($interval_ranges as $interval_range) {
      // Get all media entities set to expire.
      $media_entities = $this->getMediaSetToExpire($interval_range);

      if (empty($media_entities)) {
        continue;
      }

      $module = 'media_expire_email';
      $key = 'media_expire_email';
      $send = TRUE;

      // Send email one by one for the media set to expire.
      foreach ($media_entities as $media_entity) {
        $params['media'] = $media_entity;
        $params['email_interval'] = $interval_range['email_interval'];
        $langcode = $media_entity->get('langcode')->value;

        // Get Media author details.
        $media_author = $this->entityTypeManager->getStorage('user')
          ->load($media_entity->getOwnerId());
        $to_email = $media_author->getEmail();

        // Send email.
        $send_email = $this->mailManager
          ->mail($module, $key, $to_email, $langcode, $params, NULL, $send);

        if ($send_email['result'] != TRUE) {
          // Log email failure errors.
          $this->loggerFactory->get('media_expire_email')
            ->error('Media expire mail could not be sent to @mail', [
              '@mail' => $to_email,
            ]);
        }
      }
    }

    $this->setLastRunCheck();
  }

  /**
   * Get From and to date range to query the media entities.
   *
   * @return array
   *   from and to ranges to apply the query.
   */
  protected function getIntervalRange() {
    $email_interval = $this->mediaExpireEmailSettings
      ->get('media_expire_email_interval');

    $range = [];
    // Start from last cron execution time.
    $last_cron_executed_time = $this->state
      ->get('media_expire_email.last_run_check', REQUEST_TIME);

    foreach ($email_interval as $interval_key => $interval) {
      if ($interval) {
        $from_time = strtotime("+$interval days", $last_cron_executed_time);
        $to_time = strtotime("+$interval days");
        $timeNow = new DrupalDateTime('now', DateTimeItemInterface::STORAGE_TIMEZONE);

        $range[$interval_key]['email_interval'] = $interval;
        $range[$interval_key]['from'] = $timeNow
          ->setTimestamp($from_time)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $range[$interval_key]['to'] = $timeNow
          ->setTimestamp($to_time)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      }
    }

    return $range;
  }

  /**
   * Get Media details set to expire in configured duration.
   *
   * @param array $interval_range
   *   Interval Range.
   *
   * @return object
   *   Media entities
   */
  protected function getMediaSetToExpire(array $interval_range) {
    $bundles = $this->entityTypeManager->getStorage('media_type')
      ->loadMultiple();

    $media_entities = [];
    foreach ($bundles as $bundle) {
      if ($bundle->getThirdPartySetting('media_expire', 'enable_expiring')) {
        $expireField = $bundle->getThirdPartySetting('media_expire', 'expire_field');
        $query = $this->entityTypeManager->getStorage('media')->getQuery('AND');
        $query->condition('status', 1);
        $query->condition('bundle', $bundle->id());
        $query->condition($expireField, $interval_range['from'], '>=');
        $query->condition($expireField, $interval_range['to'], '<=');

        $entityIds = $query->execute();
        // Load all media entities set to expire.
        $media_entities = $this->entityTypeManager->getStorage('media')
          ->loadMultiple($entityIds);
      }
    }

    return $media_entities;
  }

  /**
   * Set time to start from this point next time the cron runs.
   */
  protected function setLastRunCheck() {
    $this->state->set('media_expire_email.last_run_check', REQUEST_TIME);
  }

}
