<?php

namespace Drupal\media_expire_email\Form;

/**
 * @file
 * Contains Drupal\media_expire_email\Form\EmailSettingsForm.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\token\TreeBuilderInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Media Expire Email Configuration form.
 *
 * @package Drupal\media_expire_email\Form
 */
class EmailSettingsForm extends ConfigFormBase {

  /**
   * The token tree builder.
   *
   * @var \Drupal\token\TreeBuilderInterface
   */
  protected $treeBuilder;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(TreeBuilderInterface $tree_builder,
                              RendererInterface $renderer) {
    $this->treeBuilder = $tree_builder;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('token.tree_builder'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_expire_email.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_expire_email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_expire_email.settings');

    $form['media_expire_email_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Media Expire Email Alert'),
      '#default_value' => $config->get('media_expire_email_enabled') ?? FALSE,
    ];

    $form['email_template'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email Template'),
    ];

    $form['email_template']['media_expire_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Media Expire Email Subject'),
      '#default_value' => $config->get('media_expire_email_subject') ?? '',
    ];

    // Show available tokens for media and media expire email.
    $token_tree = $this->treeBuilder
      ->buildRenderable(['media', 'media_expire_email'], ['global_types' => FALSE]);

    if (!empty($token_tree)) {
      $message_prefix = $this->renderer->render($token_tree);
    }

    $form['email_template']['media_expire_email_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Media Expire Email Message'),
      '#description' => $this->t('Enter Email message for media expire'),
      '#suffix' => $message_prefix ?? '',
      '#default_value' => $config->get('media_expire_email_message') ?? '',
    ];

    $form['email_interval'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email Interval'),
      '#description' => $this->t('You can set multiple intervals for email alert in days.'),
    ];

    // Retrieve the existing blacklist and initiatlize the counter.
    $email_interval = $config->get('media_expire_email_interval');
    if (is_null($form_state->get('email_interval_items_count'))) {
      if (empty($email_interval)) {
        $form_state->set('email_interval_items_count', 1);
      }
      else {
        $form_state->set('email_interval_items_count', count($email_interval));
      }
    }

    // Define the fields based on whats stored in form state.
    $max = $form_state->get('email_interval_items_count');
    $form['email_interval']['media_expire_email_interval'] = [
      '#tree' => TRUE,
      '#prefix' => '<div id="email-interval-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($delta = 0; $delta < $max; $delta++) {
      if (!isset($form['email_interval']['media_expire_email_interval'][$delta])) {
        $element = [
          '#type' => 'number',
          '#default_value' => isset($email_interval[$delta]) ? $email_interval[$delta] : '',
        ];
        $form['email_interval']['media_expire_email_interval'][$delta] = $element;
      }
    }

    // Define the add button.
    $form['email_interval']['add'] = [
      '#type' => 'submit',
      '#name' => 'add',
      '#value' => $this->t('Add More'),
      '#submit' => [[$this, 'addMoreSubmit']],
      '#ajax' => [
        'callback' => [$this, 'addMoreCallback'],
        'wrapper' => 'email-interval-wrapper',
        'effect' => 'fade',
      ],
    ];

    // Define the remove button.
    $form['email_interval']['remove'] = [
      '#type' => 'submit',
      '#name' => 'remove',
      '#value' => $this->t('Remove Item'),
      '#submit' => [[$this, 'removeItemSubmit']],
      '#ajax' => [
        'callback' => [$this, 'removeItemCallback'],
        'wrapper' => 'email-interval-wrapper',
        'effect' => 'fade',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Let the form rebuild the Email Interval fields.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addMoreSubmit(array &$form, FormStateInterface $form_state) {
    $count = $form_state->get('email_interval_items_count');
    $count++;
    $form_state->set('email_interval_items_count', $count);
    $form_state->setRebuild();
  }

  /**
   * Adds more number fields to the email interval fieldset.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['email_interval']['media_expire_email_interval'];
  }

  /**
   * Let the form rebuild the Email Interval fields.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function removeItemSubmit(array &$form, FormStateInterface $form_state) {
    $count = $form_state->get('email_interval_items_count');
    // Do not remove only one item exists.
    if ($count > 1) {
      $count--;
    }

    $form_state->set('email_interval_items_count', $count);
    $form_state->setRebuild();
  }

  /**
   * Adds more number fields to the email interval fieldset.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function removeItemCallback(array &$form, FormStateInterface $form_state) {
    return $form['email_interval']['media_expire_email_interval'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data_to_save = [
      'media_expire_email_enabled',
      'media_expire_email_subject',
      'media_expire_email_message',
      'media_expire_email_interval',
    ];

    foreach ($data_to_save as $data_item) {
      $this->config('media_expire_email.settings')
        ->set($data_item, $form_state->getValue($data_item))
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
